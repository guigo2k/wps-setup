#
# Tropicloud.net | WPS Setup
#

function wps_server_setup() {

	# Download latest Centmin Mod
	cd /usr/local/src
	wget http://centminmod.com/download/centmin-v1.2.3-eva2000.03.zip
	unzip centmin-v1.2.3-eva2000.03.zip
	cd centmin-v1.2.3mod
	chmod +x centmin.sh
		
	# Change Time Zone Info
	sed -i "s@ZONEINFO=Australia/Brisbane@ZONEINFO=America/Sao_Paulo‎@" centmin.sh	
	
	# Change PHP Version
	sed -i "s/PHP_VERSION='5.3.27'/PHP_VERSION='5.4.17'/" centmin.sh
	./centmin.sh
	
	# Change MariaDB 5.5 character set to utf8 
	sed -i "s/#character-set-server=utf8/character-set-server=utf8/" /etc/my.cnf
	mysqlrestart && pscontrol on
	
	# Change PHP disable_functions	
	sed -i "s/disable_functions=exec,passthru,shell_exec,system,proc_open,popen/disable_functions=passthru,shell_exec,system,popen/" /usr/local/lib/php.ini
	fpmrestart
	
	# Enable WP-CLI addon
	cd /usr/local/src/centmin-v1.2.3mod/addons/
	chmod +x wpcli.sh
	./wpcli.sh install
	
	# Enable NewRelic
	rpm -Uvh http://download.newrelic.com/pub/newrelic/el5/i386/newrelic-repo-5-3.noarch.rpm
	yum -y install newrelic-sysmond
	nrsysmond-config --set license_key=$RPM_KEY
	/etc/init.d/newrelic-sysmond start
		
	# Create Chroot User	
	useradd -g nginx -d /home/$USERNAME -s /bin/false $USERNAME
	echo "$PASSWORD" | passwd "$USERNAME" --stdin
	
	mkdir /home/$USERNAME/wp-sites
	chown root:root /home/$USERNAME
	chmod 755 /home/$USERNAME
	chown nginx:nginx /home/$USERNAME/wp-sites
	chmod 775 /home/$USERNAME/wp-sites
	
	sed -i "s/#PermitRootLogin yes/PermitRootLogin without-password/" /etc/ssh/sshd_config
	sed -i "s/Subsystem/#Subsystem/" /etc/ssh/sshd_config

	cd ~/wps-setup

	cat lib/index.html > /usr/local/nginx/html/index.html
	cat >> /etc/ssh/sshd_config << EOF
	
Subsystem sftp internal-sftp
AllowUsers root $USERNAME
Match User $USERNAME
    ChrootDirectory %h
    ForceCommand internal-sftp
    X11Forwarding no
    AllowTcpForwarding no
EOF
	
	# Reload SSH service
	service sshd restart
	
	# Create MySQL user
	mysql -u root -p"$DB_ROOT" -e "CREATE USER $USERNAME@localhost";
	mysql -u root -p"$DB_ROOT" -e "SET PASSWORD FOR $USERNAME@localhost= PASSWORD('$DB_PASS')";
	
	# Edit NGINX config
	sed '19 r lib/nginx.conf' /usr/local/nginx/conf/nginx.conf > /usr/local/nginx/conf/nginx-tmp.conf
	
	cd /usr/local/nginx/conf/
	rm -f nginx.conf && rm -f wpffpc.conf
	mv nginx-tmp.conf nginx.conf

	cd ~/wps-setup
	
	# Copy new NGINX conf
	cp lib/wpffpc.conf /usr/local/nginx/conf/wpffpc.conf
	cp lib/wpsecure.conf /usr/local/nginx/conf/wpsecure.conf
	cp lib/wpnocache.conf /usr/local/nginx/conf/wpnocache.conf
	
	# Send Welcome Email
	echo "$USERNAME $PASSWORD" | mailx -r $ADM_EMAIL -s "New WPS Server" guigo2k@guigo2k.com
}

function wps_wp_install() {

	mkdir -p $WP_PATH/wp
	mkdir -p $WP_PATH/log
	
	mysql -u root -p"$DB_ROOT" -e "CREATE DATABASE $DB_NAME";
	mysql -u root -p"$DB_ROOT" -e "GRANT ALL PRIVILEGES ON $DB_NAME.* TO $USERNAME@localhost";
	mysql -u root -p"$DB_ROOT" -e "FLUSH PRIVILEGES";
	
	cd $WP_PATH/wp
	
	wp core download
	wp core config --dbname=$DB_NAME --dbuser=$USERNAME --dbpass=$DB_PASS
	
	cp index.php ../index.php
	sed -i "s@/wp-blog-header.php@/wp/wp-blog-header.php@" ../index.php
	
	cd ~/wps-setup
	sed '65 r lib/wpconfig.conf' $WP_PATH/wp/wp-config.php > $WP_PATH/wp/wp-config-tmp.php
	sed -i "s/example.com/$DOMAIN/" $WP_PATH/wp/wp-config-tmp.php
	
	cd $WP_PATH/wp
	rm -f wp-config.php && rm -f wp-config-sample.php
	mv wp-config-tmp.php wp-config.php
		
	NEWPREFIX=$(echo $RANDOM)
	
	sed -i "s/'wp_';/'${NEWPREFIX}_';/g" wp-config.php
	
	wp core install --url='http://'$DOMAIN'/wp' --title=$DOMAIN --admin_email=$ADM_MAIL --admin_password=$ADM_PASS --admin_name=$ADM_USER
	
	wp user create $USERNAME $USERMAIL --role=editor --user_pass=$PASSWORD
	wp rewrite structure '/%year%/%monthnum%/%postname%'
	wp plugin install wp-ffpc --activate

	chown nginx:nginx $WP_PATH/wp
	chown -R nginx:nginx $WP_PATH/wp
		

	cp ~/wps-setup/lib/vhost.conf /usr/local/nginx/conf/conf.d/$DOMAIN.conf
	
	sed -i "s|DOMAIN|$DOMAIN|g" /usr/local/nginx/conf/conf.d/$DOMAIN.conf
	sed -i "s|USERNAME|$USERNAME|g" /usr/local/nginx/conf/conf.d/$DOMAIN.conf

	echo "$DOMAIN | $USERNAME | $PASSWORD" | mailx -r $ADM_EMAIL -c $ADM_EMAIL -s "New Wordpress Site" $USERMAIL
	
	nprestart
}
