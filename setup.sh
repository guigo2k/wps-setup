#! /bin/bash

#
# Tropicloud.net | WPS Setup
#
# @author: Guilherme Ribeiro
# @author: http://www.guigo2k.com
# version: 0.5
#
#
# HOW TO INSTALL
#
# 1) yum -y install git bc mailx
# 2) git clone https://guigo2k:2532xd9f@bitbucket.org/guigo2k/wps-setup.git
# 3) ln -s wps-setup/setup.sh ~/wps
#

cd ~/wps-setup

if [ $1 = server -a $2 = setup ]

	then
	printf "User email address: "
	read USERMAIL
	
	. wps-config.sh
	. wps-functions.sh
	
	wps_server_setup
	
	echo "WPS setup successful!";
	echo "Listening at http://$WPSIP4/";
	
elif [ $1 = centminmod -a $2 = menu ]

	then 
	cd /usr/local/src/centmin-v1.2.3mod
	./centmin.sh
	
elif [ $1 = install -a $2 = wordpress ]

	then
	printf "Domain: "
	read DOMAIN
	
	printf "User email address: "
	read USERMAIL
	
	. wps-config.sh
	. wps-functions.sh
	
	wps_wp_install
	
	echo "Successful Wordpress Install.";
	echo "Visit your site at $DOMAIN.";	
fi